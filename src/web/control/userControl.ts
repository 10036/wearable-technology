
import {
  MAKAI_ERROR_EMPTY_TOKEN,
  MAKAI_ERROR_INTERNET,
  MAKAI_ERROR_INVALID_TOKEN,
  MAKAI_ERROR_TOKEN_NOT_EXIST,
  MAKAI_ERROR_EXIST_DISPLAYNAME,
  MAKAI_ERROR_INVALID_EMAIL,
  MAKAI_ERROR_EXIST_EMAIL,
  MAKAI_ERROR_EMAIL_NOT_EXIST,
  MAKAI_INFO_CONFIRM_TOKEN,
  MAKAI_INFO_SET_TOKKEN_SUCCESS,
  MAKAI_INFO_CHANGING_SUCCESS,
  MAKAI_MODAL_CANCEL,
  MAKAI_MODAL_CONTENT_DEVELOPMENT_HINT,
  MAKAI_MODAL_CONTENT_MAKAI_TOKEN_DESC,
  MAKAI_MODAL_CONTENT_TOKEN_INPUT_PREFIX,
  MAKAI_MODAL_OK,
  MAKAI_MODAL_SAVE,
  MAKAI_MODAL_GO_TO_GRAVATAR,
  MAKAI_MODAL_TITLE_INFO,
  MAKAI_MODAL_TITLE_TOKEN,
  MAKAI_MODAL_TITLE_WAITING,
  MAKAI_MODAL_TITLE_DISPLAYNAME,
  MAKAI_MODAL_TITLE_EMAIL,
  MAKAI_MODAL_TITLE_AVATAR,
  MAKAI_MODAL_CONTENT_DISPLAYNAME_DESC,
  MAKAI_MODAL_CONTENT_EMAIL_DESC,
  MAKAI_MODAL_CONTENT_AVATAR_DESC,
  MAKAI_INFO_CHANGING
} from '../constant/messages';
import { h } from '../hs';
import { getToken, makaiUrl, saveToken, saveUsername, hasToken, getUsername, getDisplayName, getEmail } from './makaiControl';
import { Modal } from './modalControl';
import { MakaiMenu } from '../menu/MakaiMenu';

export function showMessage(message: string) {
  const modal = new Modal(h('div', [
    h('h1', MAKAI_MODAL_TITLE_INFO),
    h('p', message),
    h('.button-container', [
      h('div', {
        onclick: () => {
          modal.close();
        }
      }, MAKAI_MODAL_OK),
    ]),
  ]));
  modal.setDismissible();
  modal.open();
}

export function showLoading(message: string): Modal {
  const m = new Modal(h('div', [
    h('h1', MAKAI_MODAL_TITLE_WAITING),
    h('p', message),
  ]));
  m.open();
  return m;
}

export function showLogin() {
  const $token: HTMLInputElement = h('input', {
    value: getToken() === undefined ? '' : getToken()
  }) as HTMLInputElement;
  const modal = new Modal(h('div', [
    h('h1', MAKAI_MODAL_TITLE_TOKEN),
    ...MAKAI_MODAL_CONTENT_MAKAI_TOKEN_DESC.split('\n').map(p => h('p', p)),
    h('i', MAKAI_MODAL_CONTENT_DEVELOPMENT_HINT),
    h('.input-group', [
      h('span', MAKAI_MODAL_CONTENT_TOKEN_INPUT_PREFIX),
      $token,
    ]),
    h('.button-container', [
      h('div', {
        onclick: () => {
          if ($token.value === '') {
            showMessage(MAKAI_ERROR_EMPTY_TOKEN);
            return;
          }
          const m = showLoading(MAKAI_INFO_CONFIRM_TOKEN);
          fetch(makaiUrl + '/username/' + $token.value).then((response) => {
            return response.json();
          })
            .then((json) => {
              m.close();
              if (json.username === null) {
                showMessage(MAKAI_ERROR_INVALID_TOKEN);
              } else {
                saveToken($token.value);
                saveUsername(json.username);
                modal.close();
                showMessage(MAKAI_INFO_SET_TOKKEN_SUCCESS);
              }
            }).catch((err) => {
              m.close();
              showMessage(MAKAI_ERROR_INTERNET);
            });
        }
      }, MAKAI_MODAL_SAVE),
      h('div', {
        onclick: () => {
          modal.close();
        }
      }, MAKAI_MODAL_CANCEL),
    ]),
  ]));
  modal.setDismissible();
  modal.open();
}

export function showSetDisplayName() {
  if (!hasToken()) {
    showMessage(MAKAI_ERROR_TOKEN_NOT_EXIST);
    return;
  }
  const $name: HTMLInputElement = h('input', { value: getDisplayName() === undefined ? '' : getDisplayName() }) as HTMLInputElement;
  const modal = new Modal(h('div', [
    h('h1', MAKAI_MODAL_TITLE_DISPLAYNAME),
    ...MAKAI_MODAL_CONTENT_DISPLAYNAME_DESC.split('\n').map(p => h('p', p)),
    h('.input-group', [
      $name,
    ]),
    h('.button-container', [
      h('div', {
        onclick: () => {
          if ($name.value === '') {
            showMessage(MAKAI_ERROR_EMPTY_TOKEN);
            return;
          }
          const m = showLoading(MAKAI_INFO_CHANGING);
          fetch(makaiUrl + '/displayname/' + getToken(), {
            body: JSON.stringify({ username: $name.value }),
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: new Headers({
              'Content-Type': 'application/json'
            }),
            method: 'POST',
            mode: 'cors',
            redirect: 'follow',
            referrer: 'no-referrer',
          }).then((response) => {
            return response.json();
          })
            .then((json) => {
              m.close();
              if (json.success) {
                showMessage(MAKAI_INFO_CHANGING_SUCCESS);
                localStorage.setItem('displayName', $name.value);
              } else {
                showMessage(MAKAI_ERROR_EXIST_DISPLAYNAME);
              }
            }).catch((err) => {
              m.close();
              showMessage(MAKAI_ERROR_INTERNET);
            });
        }
      }, MAKAI_MODAL_SAVE),
      h('div', {
        onclick: () => {
          modal.close();
        }
      }, MAKAI_MODAL_CANCEL),
    ]),
  ]));
  modal.setDismissible();
  modal.open();
}
export function showSetEmail() {
  if (!hasToken()) {
    showMessage(MAKAI_ERROR_TOKEN_NOT_EXIST);
    return;
  }
  const $name: HTMLInputElement = h('input', { value: getEmail() === undefined ? '' : getEmail() }) as HTMLInputElement;
  const modal = new Modal(h('div', [
    h('h1', MAKAI_MODAL_TITLE_EMAIL),
    ...MAKAI_MODAL_CONTENT_EMAIL_DESC.split('\n').map(p => h('p', p)),
    h('.input-group', [
      $name,
    ]),
    h('.button-container', [
      h('div', {
        onclick: () => {
          if ($name.value === '') {
            showMessage(MAKAI_ERROR_EMPTY_TOKEN);
            return;
          }
          const m = showLoading(MAKAI_INFO_CHANGING);
          fetch(makaiUrl + '/email/' + getToken(), {
            body: JSON.stringify({ email: $name.value }),
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: new Headers({
              'Content-Type': 'application/json'
            }),
            method: 'POST',
            mode: 'cors',
            redirect: 'follow',
            referrer: 'no-referrer',
          }).then((response) => {
            return response.json();
          })
            .then((json) => {
              m.close();
              if (json.success) {
                showMessage(MAKAI_INFO_CHANGING_SUCCESS);
                localStorage.setItem('email', $name.value);
              } else {
                switch (json.errorMessage) {
                  case 'Illegal email format.':
                    showMessage(MAKAI_ERROR_INVALID_EMAIL);
                    break;
                  default:
                    showMessage(MAKAI_ERROR_EXIST_EMAIL);
                    break;
                }
              }
            }).catch((err) => {
              m.close();
              showMessage(MAKAI_ERROR_INTERNET);
            });
        }
      }, MAKAI_MODAL_SAVE),
      h('div', {
        onclick: () => {
          modal.close();
        }
      }, MAKAI_MODAL_CANCEL),
    ]),
  ]));
  modal.setDismissible();
  modal.open();
}

export function showSetAvatar() {
  const modal = new Modal(h('div', [
    h('h1', MAKAI_MODAL_TITLE_AVATAR),
    ...MAKAI_MODAL_CONTENT_AVATAR_DESC.split('\n').map(p => h('p', p)),
    h('a.regular', {
      href: 'https://gravatar.com/',
      target: '_blank',
    }, MAKAI_MODAL_GO_TO_GRAVATAR),
    h('.button-container', [
      h('div', {
        onclick: () => {
          modal.close();
        }
      }, MAKAI_MODAL_OK),
    ]),
  ]));
  modal.setDismissible();
  modal.open();
}
